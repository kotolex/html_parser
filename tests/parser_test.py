from unittest import TestCase, main

from py_parse import Nodes
from py_parse.base import MyHTMLParser
from py_parse import Node
from py_parse import WrongHTMLFormatError
from py_parse import Parser

contents = [None, None, None, None]


def file_content(name: str) -> str:
    value = contents[0] if name == 'test' else contents[1]
    if not value:
        with open(f'tests/{name}.html', encoding='utf-8') as file:
            value = ''.join(e.rstrip() for e in file)
        if name == 'test':
            contents[0] = value
        elif name == 'test2':
            contents[1] = value
        elif name == 'test3':
            contents[2] = value
        else:
            contents[3] = value
    return value


CASE_01 = '<div class="raw">Высота: 1<br>Ширина: 2<br>Длина: 4</div>'


class TestParser(TestCase):
    def test_simple_parse(self):
        node =Parser().parse('<html></html>')[0]
        expected = Node('html')
        self.assertEqual(expected, node)

    def test_simple_parse_data(self):
        node =Parser().parse('<html>Text</html>')[0]
        expected = Node('html', text="Text")
        self.assertEqual(expected, node)

    def test_simple_parse_attr(self):
        node =Parser().parse('<div class="new"></div>')[0]
        expected = Node('div', attrs=[('class', 'new')])
        self.assertEqual(expected, node)

    def test_simple_parse_attr_and_data(self):
        node =Parser().parse('<div class="new">Text</div>')[0]
        expected = Node('div', text='Text', attrs=[('class', 'new')])
        self.assertEqual(expected, node)

    def test_parse_parent_and_child(self):
        node =Parser().parse('<div class="new">Text<span>Text2</span></div>')[0]
        expected_child = Node('span', text='Text2')
        expected_parent = Node('div', text='Text', attrs=[('class', 'new')])
        self.assertEqual(node._children[0], expected_child)
        self.assertEqual(node._children[0]._parent, expected_parent)

    def test_parse_two_levels(self):
        node =Parser().parse('<div>Text<span>Text2<p>Text3</p></span></div>')[0]
        expected_child1 = Node('span', text='Text2')
        expected_child2 = Node('p', text='Text3')
        expected_parent = Node('div', text='Text')
        self.assertEqual(node._children[0], expected_child1)
        self.assertEqual(node._children[0]._children[0], expected_child2)
        self.assertEqual(node._children[0]._parent, expected_parent)
        self.assertEqual(node._children[0]._children[0]._parent, expected_child1)

    def test_parse_siblings(self):
        node =Parser().parse('<div>Text<span>Text2</span><p>Text3</p></div>')[0]
        expected_child1 = Node('span', text='Text2')
        expected_child2 = Node('p', text='Text3')
        expected_parent = Node('div', text='Text')
        self.assertEqual(node._children[0], expected_child1)
        self.assertEqual(node._children[1], expected_child2)
        self.assertEqual(node._children[0]._parent, expected_parent)
        self.assertEqual(node._children[1]._parent, expected_parent)
        self.assertEqual(node._children[0]._next_node, expected_child2)
        self.assertEqual(node._children[1]._prev_node, expected_child1)

    def test_parse_html(self):
        h = '<html><head><title>Test</title></head><body><h1>Parse me!</h1><div class="some">Text</div></body></html>'
        text = 'Node<tag=html, previous=None, next=None, attributes=None, children=Nodes<length=2>, ' \
               'parent=None>\n    Node<tag=head, previous=None, next=<body></body>, attributes=None, ' \
               'children=Nodes<length=1>, parent=<html></html>>\n        Node<tag=title, previous=None, next=None, ' \
               'attributes=None, text=Test, children=Nodes<length=0>, parent=<head></head>>\n    Node<tag=body, ' \
               'previous=<head></head>, next=None, attributes=None, children=Nodes<length=2>, ' \
               'parent=<html></html>>\n        Node<tag=h1, previous=None, next=<div class="some">Text</div>, ' \
               'attributes=None, text=Parse me!, children=Nodes<length=0>, parent=<body></body>>\n        ' \
               'Node<tag=div, previous=<h1>Parse me!</h1>, next=None, attributes=[(\'class\', \'some\')], text=Text, ' \
               'children=Nodes<length=0>, parent=<body></body>>'
        self.assertEqual(Parser().parse(h).html(), text)

    def test_parse_html_nodes(self):
        h = '<html><head><title>Test</title></head><body><h1>Parse me!</h1><div class="some">Text</div></body></html>'
        node =Parser().parse(h)[0]
        exp_html = Node(tag='html')
        self.assertEqual(node, exp_html)
        exp_head = Node(tag='head')
        exp_body = Node(tag='body')
        exp_title = Node(tag='title', text='Test')
        exp_h1 = Node(tag='h1', text='Parse me!')
        exp_div = Node(tag='div', text='Text', attrs=[('class', 'some')])
        self.assertIsNone(node._prev_node)
        self.assertIsNone(node._next_node)
        self.assertEqual(node._children[0], exp_head)
        self.assertEqual(node._children[1], exp_body)
        self.assertEqual(node._children[0]._parent, exp_html)
        self.assertEqual(node._children[1]._parent, exp_html)
        self.assertEqual(node._children[0]._next_node, exp_body)
        self.assertEqual(node._children[1]._prev_node, exp_head)
        self.assertIsNone(node._children[0]._prev_node)
        self.assertIsNone(node._children[1]._next_node)
        self.assertEqual(node._children[0]._children[0], exp_title)
        self.assertIsNone(node._children[0]._children[0]._next_node)
        self.assertIsNone(node._children[0]._children[0]._prev_node)
        self.assertEqual(node._children[0]._children[0]._parent, exp_head)
        self.assertEqual(node._children[1]._children[0], exp_h1)
        self.assertEqual(node._children[1]._children[1], exp_div)
        self.assertEqual(node._children[1]._children[0]._parent, exp_body)
        self.assertEqual(node._children[1]._children[1]._parent, exp_body)
        self.assertEqual(node._children[1]._children[0]._next_node, exp_div)
        self.assertEqual(node._children[1]._children[1]._prev_node, exp_h1)
        self.assertIsNone(node._children[1]._children[0]._prev_node)
        self.assertIsNone(node._children[1]._children[1]._next_node)

    def test_parse_error_no_closed_tags(self):
        with self.assertRaises(WrongHTMLFormatError) as e:
           Parser().parse('<html><body>')
        self.assertEqual(str(e.exception), 'Some tags was not closed itself or by parents, count= 2')

    def test_parse_error_only_closed_tags(self):
        with self.assertRaises(WrongHTMLFormatError) as e:
           Parser().parse('</html>')
        self.assertEqual(str(e.exception), 'Closed tag (html) without opening!')

    def test_parse_error_only_closed_tags_with_message(self):
        with self.assertRaises(WrongHTMLFormatError) as e:
           Parser().parse('<div>', show_unclosed_tags=True)
        self.assertEqual(str(e.exception), 'Some tags was not closed itself or by parents, count= 1')

    def test_parse_self_closed(self):
        node =Parser().parse('<img src="yandex"/>')[0]
        expected = Node('img', attrs=[('src', 'yandex')])
        self.assertEqual(node, expected)

    def test_parse_ya_ru(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        self.assertEqual(1, len(nodes))
        self.assertEqual(58, len(nodes._flatten()))

    def test_parse_ya_ru_ancestor(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'noscript').ancestor(lambda x: x.tag == 'html')
        str_rep = '<html class="i-ua_js_no i-ua_css_standart i-ua_browser_chrome i-ua_browser-engine_webkit m-stat ' \
                  'i-ua_browser_desktop i-ua_platform_windows" lang="ru"></html>'
        self.assertEqual(str_rep, str(n))

    def test_parse_ya_ru_descendant(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'html').descendant(lambda x: x.tag == 'noscript')
        str_rep = '<noscript></noscript>'
        self.assertEqual(str_rep, str(n))

    def test_parse_ya_ru_child(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'table').child(lambda x: 'layout__search' in x.class_)
        str_rep = '<tr class="b-table__row layout__search"></tr>'
        self.assertEqual(str_rep, str(n))

    def test_parse_ya_ru_next_sibling(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'table').next_sibling(lambda x: x.tag == 'noscript')
        str_rep = '<noscript></noscript>'
        self.assertEqual(str_rep, str(n))

    def test_parse_ya_ru_prev_sibling(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'noscript').prev_sibling(lambda x: x.tag == 'table')
        str_rep = '<table class="b-table layout__table"></table>'
        self.assertEqual(str_rep, str(n))

    def test_parse_ya_ru_find_all_divs(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find_all(lambda x: x.tag == 'div')
        self.assertTrue(len(n), 6)

    def test_parse_ya_ru_find_first_div(self):
        nodes =Parser().parse(file_content('test'), show_unclosed_tags=True)
        n = nodes.find(lambda x: x.tag == 'div')
        str_rep = '<div class="personal i-bem" data-bem="{"personal":' \
                  '{"authUrl":"https://yandex.ru/data/mail.js?yaru=y","retpath":"https%3A%2F%2Fya.ru","stat":1}}">' \
                  '</div>'
        self.assertEqual(str(n), str_rep)

    def test_parse_attrs(self):
        node =Parser().parse('<a class="headerlink" href="#html.parser.HTMLParser" title="Permalink to this">Test</a>')[0]
        self.assertEqual('headerlink', node.class_)
        self.assertEqual('#html.parser.HTMLParser', node.href)
        self.assertEqual('a', node.tag)
        self.assertEqual('Permalink to this', node.title)
        self.assertEqual('Test', node.text)

    def test_parse_comments(self):
        parser = Parser()
        node =parser.parse('<div><!--noindex--></div>')
        self.assertEqual(['noindex'], parser.comments)

    def test_parse_no_error_on_wrong_markup(self):
       Parser().parse(file_content('test2'))

    def test_parse_ok_on_relaxed(self):
        nodes =Parser().parse(file_content('test2'), strict_parsing=False)
        self.assertEqual(15, len(nodes))

    def test_parse_ok_on_auto_closed(self):
        nodes =Parser().parse(file_content('test2'), auto_close=['frame'])
        self.assertEqual(Node('h2', text='Frame Alert'), nodes.find(lambda x: x.tag == 'h2'))

    def test_parse_text(self):
        nodes =Parser().parse('<p>Link to <a href="overview-summary.html">Non-frame version</a>.</p>')
        self.assertEqual('Link to \n.', nodes[0].text)
        self.assertEqual('Non-frame version', nodes[0].child().text)

    def test_parse_auto_closed(self):
        nodes =Parser().parse('<d src="text" />')
        self.assertEqual('<d src="text"></d>', str(nodes[0]))
        self.assertTrue(nodes[0].closed)

    def test_parse_case01_ok(self):
        node =Parser().parse(CASE_01).find_tag('div')
        self.assertEqual('<div class="raw">Высота: 1\nШирина: 2\nДлина: 4</div>', str(node))

    def test_parse_case01_failed(self):
        node =Parser().parse(CASE_01).find_tags('img')
        self.assertEqual(Nodes(), node)

    def test_parse_case01_ok_with_filter(self):
        node =Parser().parse(CASE_01).find_tag('div', lambda e: e.text and 'Высота' in e.text)
        self.assertEqual('<div class="raw">Высота: 1\nШирина: 2\nДлина: 4</div>', str(node))

    def test_parse_case01_failed_with_filter(self):
        node =Parser().parse(CASE_01).find_tags('div', lambda e: e.text and 'WRONG' in e.text)
        self.assertEqual(Nodes(), node)

    def test_parse_case01_text(self):
        node =Parser().parse(CASE_01).find_tag('div')
        self.assertEqual('Высота: 1\nШирина: 2\nДлина: 4', node.text)

    def test_parse_case01_cleared_text(self):
        node =Parser().parse(CASE_01).find_tag('div')
        self.assertEqual('Высота: 1\nШирина: 2\nДлина: 4', node.cleared_text)

    def test_parse_case01_tags_with_attr(self):
        node =Parser().parse(CASE_01).find_tag('div', lambda e: e.class_ == 'raw')
        self.assertEqual('<div class="raw">Высота: 1\nШирина: 2\nДлина: 4</div>', str(node))

    def test_parse_case01_tags_with_attr_failed(self):
        nodes =Parser().parse(CASE_01).find_tags('div', lambda e: e.any == 'raw')
        self.assertEqual(Nodes(), nodes)

    def test_parse_unclosed_tag_gets_it(self):
        pars_parent = Parser()
        parser = MyHTMLParser(pars_parent)
        parser.feed('<div class="g"><span class="s"></div>')
        node = Node('span', attrs=[('class', 's')])
        self.assertEqual(Nodes(node), pars_parent.unclosed_nodes)

    def test_parse_unclosed_tag_two_in_raw(self):
        pars_parent = Parser()
        parser = MyHTMLParser(pars_parent)
        parser.feed('<div class="g"><span class="s1"></span><span class="s2"><span class="s"></div>')
        node = Node('span', attrs=[('class', 's')])
        node2 = Node('span', attrs=[('class', 's2')])
        self.assertEqual(Nodes(node, node2), pars_parent.unclosed_nodes)

    def test_parse_unclosed_tag_empty_if_ok(self):
        pars_parent = Parser()
        parser = MyHTMLParser(pars_parent)
        parser.feed('<div class="g"></div>')
        self.assertTrue(pars_parent.unclosed_nodes.is_empty())

    def test_parse_failed_when_no_open_tag_for_current(self):
        with self.assertRaises(WrongHTMLFormatError) as e:
           Parser().parse('<div class="g"></span>')
        self.assertEqual(str(e.exception), 'Cant find open tag for closed tag "span"')

    def test_parse_equalfor_find_all_and_find_tags(self):
        nodes =Parser().parse(file_content('test4'))
        self.assertEqual(nodes.find_tags('div', lambda e: 'post_message' in e.id),
                         nodes.find_all(lambda e: e.tag == 'div' and 'post_message' in e.id))

    def test_parse_cleared_text(self):
        nodes =Parser().parse(file_content('test4'))
        text = f'Добрый вечер,\nПроблема такая: не могу выцепить карточки товаров в Citilink.\n' \
               f'Делаю следующим образом:\nполучаю страницу через bs4\n' \
               f'Не могу выцепить этот квадратик с товаром из html.\nОтрывок кода:\n' \
               f'Цель - выцепить лист с квадратами предложений.\n' \
               f'Пытался через find_all("div",class_="product_data__gtm-js product_data__pageevents-js ' \
               f'ProductCardHorizontal js--ProductCardInListing js--ProductCardInWishlist")'
        self.assertEqual(text, nodes.find_tags('div', lambda e: 'post_message' in e.id)[0].cleared_text)


if __name__ == '__main__':
    main()
