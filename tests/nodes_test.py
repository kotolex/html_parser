from unittest import TestCase, main

from py_parse.nodes import Nodes
from py_parse.node import Node
from py_parse.exceptions import NoSuchElementError


class TestNodes(TestCase):

    def test_nodes_init(self):
        nodes = Nodes()
        self.assertEqual([], nodes.container)

    def test_nodes_append(self):
        nodes = Nodes()
        node = Node('div')
        nodes.append(node)
        self.assertEqual([node], nodes.container)

    def test_nodes_is_empty(self):
        nodes = Nodes()
        self.assertTrue(nodes.is_empty())
        nodes.append(Node('div'))
        self.assertFalse(nodes.is_empty())

    def test_nodes_index(self):
        nodes = Nodes()
        node = Node('div')
        nodes.append(node)
        self.assertEqual(node, nodes[0])
        self.assertEqual(node, nodes[-1])

    def test_nodes_in(self):
        nodes = Nodes()
        node = Node('div')
        nodes.append(node)
        self.assertTrue(node in nodes)

    def test_nodes_bool(self):
        nodes = Nodes()
        self.assertFalse(nodes)
        node = Node('div')
        nodes.append(node)
        self.assertTrue(nodes)

    def test_nodes_repr(self):
        nodes = Nodes()
        node = Node('div')
        nodes.append(node)
        self.assertEqual(nodes.__repr__(), 'Nodes<length=1>')

    def test_nodes_eq(self):
        nodes = Nodes()
        nodes2 = Nodes()
        node = Node('div')
        nodes.append(node)
        nodes2.append(node)
        self.assertEqual(nodes, nodes2)

    def test_nodes_html(self):
        nodes = Nodes()
        node = Node('div')
        node2 = Node('body')
        nodes.append(node)
        nodes.append(node2)
        self.assertEqual(nodes.html(), "Node<tag=div, previous=None, next=None, attributes=None, "
                                       "children=Nodes<length=0>, parent=None>\nNode<tag=body, previous=None, "
                                       "next=None, attributes=None, children=Nodes<length=0>, parent=None>")

    def test_nodes_html_inner(self):
        nodes = Nodes()
        nodes2 = Nodes()
        node = Node('div')
        node2 = Node('div')
        node._children = nodes2
        nodes2.append(node2)
        nodes2.append(node2)
        nodes.append(node)
        self.assertEqual(nodes.html(), "Node<tag=div, previous=None, next=None, attributes=None, "
                                       "children=Nodes<length=2>, parent=None>\n    Node<tag=div, previous=None, "
                                       "next=None, attributes=None, children=Nodes<length=0>, "
                                       "parent=None>\n    Node<tag=div, previous=None, next=None, attributes=None, "
                                       "children=Nodes<length=0>, parent=None>")

    def test_nodes_flatten(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes()
        nodes.append(node)
        nodes.append(node2)
        nodes2 = Nodes(node, node2)
        self.assertEqual(nodes2, nodes._flatten())

    def test_nodes_flatten_case1(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('title')
        nodes2 = Nodes()
        nodes2.append(node3)
        node._children = nodes2
        nodes = Nodes(node, node2)
        nodes3 = Nodes(node, node3, node2)
        self.assertEqual(nodes3, nodes._flatten())

    def test_nodes_first(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(node, nodes.first())

    def test_nodes_last(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(node2, nodes.last())

    def test_nodes_find(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(node, nodes.find(lambda x: x.tag == 'div'))

    def test_nodes_find_tag(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(node, nodes.find_tag('div'))

    def test_nodes_find_tag_with_filter(self):
        node = Node('div', text='test')
        node2 = Node('h1')
        node2._parent = node
        node._children.append(node2)
        nodes = Nodes(node, node2)
        self.assertEqual(node, nodes.find_tag('div', lambda x: x.text and x.text == 'test'))

    def test_nodes_find_all(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(nodes, nodes.find_all(lambda x: x.tag == 'div' or x.tag == 'h1'))

    def test_nodes_find_error(self):
        node = Node('div')
        nodes = Nodes(node)
        with self.assertRaises(NoSuchElementError) as e:
            nodes.find(lambda x: x.tag == 'h2')
        self.assertEqual(str(e.exception), 'No elements with current filter (x.tag == \'h2\')')

    def test_nodes_find_tag_error(self):
        node = Node('div')
        nodes = Nodes(node)
        with self.assertRaises(NoSuchElementError) as e:
            nodes.find_tag('h2')
        self.assertEqual(str(e.exception), 'No elements with current filter (tag == \'h2\')')

    def test_nodes_find_tag_with_filter_error(self):
        node = Node('div')
        nodes = Nodes(node)
        with self.assertRaises(NoSuchElementError) as e:
            nodes.find_tag('h2', lambda x: x.has_parent() and x.parent.tag == 'div')
        self.assertEqual(str(e.exception), "No elements with current filter "
                                           "(tag == 'h2' and x.has_parent() and x.parent.tag == 'div')")

    def test_nodes_find_all_by_tag(self):
        node = Node('div')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(Nodes(node), nodes.find_tags('div'))
        self.assertEqual(Nodes(node2), nodes.find_tags('h1'))

    def test_nodes_find_tags_empty_if_wrong_tag(self):
        node = Node('div', text='test')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(Nodes(), nodes.find_tags('img', lambda x: x.text and x.text == 'test'))

    def test_nodes_find_tags_empty_if_wrong_filter(self):
        node = Node('div', text='test')
        node2 = Node('h1')
        nodes = Nodes(node, node2)
        self.assertEqual(Nodes(), nodes.find_tags('div', lambda x: x.text and x.text == 'wrong'))


if __name__ == '__main__':
    main()
