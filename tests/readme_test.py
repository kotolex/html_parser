from unittest import TestCase, main

from py_parse import Parser

from parser_test import file_content

content = file_content('test3')


class TestReadmeExamples(TestCase):

    def test_find_by_tag(self):
        first_strong = Parser().parse(content).find(lambda e: e.tag == 'strong')
        self.assertEqual(first_strong.text, 'Parts of the documentation:')

    def test_find_by_tag2(self):
        first_strong = Parser().parse(content).find_tag('strong')
        self.assertEqual(first_strong.text, 'Parts of the documentation:')

    def test_find_by_tag_and_text(self):
        tables = Parser().parse(content).find(
            lambda e: e.tag == 'strong' and e.text and e.text == 'Indices and tables:')
        self.assertEqual(tables.text, 'Indices and tables:')

    def test_find_by_tag_and_text2(self):
        tables = Parser().parse(content).find_tag('strong', lambda e: e.text == 'Indices and tables:')
        self.assertEqual(tables.text, 'Indices and tables:')

    def test_find_by_containing_text(self):
        copyright_ = Parser().parse(content).find(lambda e: 'pyri' in e.text)
        self.assertEqual(str(copyright_), '<a class="biglink" href="copyright.html">Copyright</a>')

    def test_find_element_with_id(self):
        element_with_id = Parser().parse(content).find(lambda e: 'id' in e)
        self.assertEqual(str(element_with_id),
                         '<script id="documentation_options" data-url_root="./" '
                         'src="_static/documentation_options.js"></script>')

    def test_find_by_tag_and_type(self):
        go = Parser().parse(content).find(lambda e: e.tag == 'input' and e.type == 'submit')
        self.assertEqual(go.value, 'Go')

    def test_find_by_tag_and_type2(self):
        go = Parser().parse(content).find_tag('input', lambda e: e.type == 'submit')
        self.assertEqual(go.value, 'Go')

    def test_find_all_scripts(self):
        scripts = Parser().parse(content).find_all(lambda e: e.tag == 'script')
        scripts = [str(script) for script in scripts]
        expected = [
            '<script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>',
            '<script src="_static/jquery.js"></script>',
            '<script src="_static/underscore.js"></script>',
            '<script src="_static/doctools.js"></script>',
            '<script src="_static/language_data.js"></script>',
            '<script src="_static/sidebar.js"></script>',
            '<script type="text/javascript" src="_static/copybutton.js"></script>',
            '<script type="text/javascript">$(\'.inline-search\').show(0);</script>',
            '<script type="text/javascript">$(\'.inline-search\').show(0);</script>',
            '<script type="text/javascript" src="_static/switchers.js"></script>'
        ]
        self.assertEqual(expected, scripts)

    def test_find_all_scripts2(self):
        scripts = Parser().parse(content).find_tags('script')
        scripts = [str(script) for script in scripts]
        expected = [
            '<script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>',
            '<script src="_static/jquery.js"></script>',
            '<script src="_static/underscore.js"></script>',
            '<script src="_static/doctools.js"></script>',
            '<script src="_static/language_data.js"></script>',
            '<script src="_static/sidebar.js"></script>',
            '<script type="text/javascript" src="_static/copybutton.js"></script>',
            '<script type="text/javascript">$(\'.inline-search\').show(0);</script>',
            '<script type="text/javascript">$(\'.inline-search\').show(0);</script>',
            '<script type="text/javascript" src="_static/switchers.js"></script>'
        ]
        self.assertEqual(expected, scripts)

    def test_find_all_by_part_of_class_name(self):
        sphinxes = Parser().parse(content).find_all(lambda e: 'sphinx' in e.class_)
        s = [str(sphinx) for sphinx in sphinxes]
        result = ['<div class="sphinxsidebar" role="navigation" aria-label="main navigation"></div>',
                  '<div class="sphinxsidebarwrapper"></div>']
        self.assertEqual(s, result)

    def test_cleared_text(self):
        clear_text = Parser().parse('<div>  One<br>\nTwo<br>\tThree   </div>').find_tag('div').cleared_text
        self.assertEqual(clear_text, 'One\nTwo\nThree')

    def test_using_function_predicate(self):
        def images_with_alt_and_style(e) -> bool:
            return e.tag == 'img' and 'alt' in e and 'middle' in e.style

        li_s = Parser().parse(content).find_all(images_with_alt_and_style)
        result = [str(li) for li in li_s]
        expected = ['<img src="_static/py.png" alt="" style="vertical-align: middle; margin-top: -1px"></img>',
                    '<img src="_static/py.png" alt="" style="vertical-align: middle; margin-top: -1px"></img>']
        self.assertEqual(result, expected)


if __name__ == '__main__':
    main()
