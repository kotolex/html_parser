from unittest import TestCase, main

from py_parse.node import Node
from py_parse.nodes import Nodes
from py_parse.exceptions import NoRelativeError


class TestNode(TestCase):
    def test_node_init_default(self):
        node = Node('div')
        self.assertFalse(any([node._parent, node._attrs, node.text, node._prev_node, node._next_node]))
        self.assertEqual(node._children, Nodes())

    def test_node_repr(self):
        node = Node('div')
        self.assertEqual(node.__repr__(), 'Node<tag=div, previous=None, next=None, attributes=None, '
                                          'children=Nodes<length=0>, parent=None>')

    def test_node_repr_with_text(self):
        node = Node('div', text='test')
        self.assertEqual(node.__repr__(), 'Node<tag=div, previous=None, next=None, attributes=None, text=test, '
                                          'children=Nodes<length=0>, parent=None>')

    def test_node_str(self):
        node = Node('div', text='text', attrs=[('class', 'test')])
        self.assertEqual(str(node), '<div class="test">text</div>')

    def test_node_html(self):
        node = Node('div', text='text', attrs=[('class', 'test')])
        self.assertEqual(node.html(), 'Node<tag=div, previous=None, next=None, attributes=[(\'class\', \'test\')], '
                                      'text=text, children=Nodes<length=0>, parent=None>')

    def test_node_flatten(self):
        node = Node('div')
        nodes = Nodes()
        nodes.append(node)
        self.assertEqual(nodes, node._flatten())

    def test_node_flatten_with_children(self):
        node = Node('div')
        node2 = Node('div')
        nodes = Nodes()
        nodes.append(node2)
        node._children = nodes
        nodes2 = Nodes(node, node2)
        self.assertEqual(nodes2, node._flatten())

    def test_node_parent(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        self.assertEqual(node2, node.parent)

    def test_node_has_parent(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        self.assertTrue(node.has_parent())
        self.assertFalse(node2.has_parent())

    def test_node_has_child(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        self.assertTrue(node2.has_child())
        self.assertFalse(node.has_child())

    def test_node_child(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        self.assertEqual(node, node2.child())

    def test_node_child_with_condition(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        node2._children.append(node3)
        self.assertEqual(node3, node2.child(lambda x: x.text == 'Text'))

    def test_node_child_with_condition_error(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        node2._children.append(node3)
        with self.assertRaises(NoRelativeError) as e:
            node2.child(lambda x: x.text == 'Text!')
        self.assertEqual(str(e.exception), 'Element <h1></h1> has no children at all')

    def test_node_descendant_is_first_child(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        node2._children.append(node3)
        self.assertEqual(node, node2.descendant())

    def test_node_descendants_are_all_inners_if_no_filter(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node4 = Node('h2', text='Text2')
        node2 = Node('h1')
        node._parent = node2
        node4._parent = node3
        node3._children.append(node4)
        node2._children.append(node)
        node2._children.append(node3)
        self.assertEqual(node, node2.descendants()[0])
        self.assertEqual(Nodes(node, node3, node4), node2.descendants())

    def test_node_descendant_with_condition(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node4 = Node('h1', text='Test')
        node2 = Node('h1')
        node._parent = node2
        node._children.append(node4)
        node2._children.append(node)
        node2._children.append(node3)
        self.assertEqual(node4, node2.descendant(lambda x: x.tag == 'h1' and x.text == 'Test'))

    def test_node_descendant_with_condition_error(self):
        node = Node('div')
        node3 = Node('div', text='Text')
        node4 = Node('h1', text='Test')
        node2 = Node('h1')
        node._parent = node2
        node._children.append(node4)
        node2._children.append(node)
        node2._children.append(node3)
        with self.assertRaises(NoRelativeError) as e:
            node2.descendant(lambda x: x.tag == 'h2')
        self.assertEqual(str(e.exception), 'Element <h1></h1> has no descendants with current filter (x.tag == \'h2\')')

    def test_node_has_next(self):
        node = Node('div')
        node2 = Node('div')
        node._next_node = node2
        self.assertTrue(node.has_next_node())
        self.assertFalse(node2.has_next_node())

    def test_node_has_prev(self):
        node = Node('div')
        node2 = Node('div')
        node._prev_node = node2
        self.assertTrue(node.has_prev_node())
        self.assertFalse(node2.has_prev_node())

    def test_node_ancestor_returns_parent_if_no_condition(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        self.assertEqual(node2, node.ancestor())

    def test_node_ancestor(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('a')
        node._parent = node2
        node2._parent = node3
        self.assertEqual(node3, node.ancestor(lambda x: x.tag == 'a'))

    def test_no_parent_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.parent()
        self.assertEqual(str(e.exception), 'Element <div></div> has no parent')

    def test_no_ancestor_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.ancestor(lambda x: x)
        self.assertEqual(str(e.exception), 'Element <div></div> has no parent')

    def test_no_ancestor_with_filter_error(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        with self.assertRaises(NoRelativeError) as e:
            node.ancestor(lambda x: x.tag == 'div')
        self.assertEqual(str(e.exception), 'Element <div></div> has no ancestor with current filter (x.tag == \'div\')')

    def test_no_child_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.child()
        self.assertEqual(str(e.exception), 'Element <div></div> has no children at all')

    def test_no_desc_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.descendant(lambda x: x)
        self.assertEqual(str(e.exception), 'Element <div></div> has no children at all')

    def test_next_sibling_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.next_sibling()
        self.assertEqual(str(e.exception), 'Element <div></div> has no following siblings')

    def test_next_sibling_error_not_found(self):
        node = Node('div')
        node2 = Node('h1')
        node._next_node = node2
        with self.assertRaises(NoRelativeError) as e:
            node.next_sibling(lambda x: x.tag == 'h2')
        self.assertEqual(str(e.exception), 'Element <div></div> has no following siblings with current filter '
                                           '(x.tag == \'h2\')')

    def test_next_sibling_returns_next_if_no_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node._next_node = node2
        self.assertEqual(node2, node.next_sibling())

    def test_next_sibling_returns_with_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('h2')
        node._next_node = node2
        node2._next_node = node3
        self.assertEqual(node3, node.next_sibling(lambda x: x.tag == 'h2'))

    def test_prev_sibling_error(self):
        node = Node('div')
        with self.assertRaises(NoRelativeError) as e:
            node.prev_sibling()
        self.assertEqual(str(e.exception), 'Element <div></div> has no preceding siblings')

    def test_prev_sibling_error_not_found(self):
        node = Node('div')
        node2 = Node('h1')
        node._prev_node = node2
        with self.assertRaises(NoRelativeError) as e:
            node.prev_sibling(lambda x: x.tag == 'h2')
        self.assertEqual(str(e.exception), 'Element <div></div> has no preceding siblings with current filter '
                                           '(x.tag == \'h2\')')

    def test_prev_sibling_returns_prev_if_no_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node._prev_node = node2
        self.assertEqual(node2, node.prev_sibling())

    def test_prev_sibling_returns_with_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('h2')
        node._prev_node = node2
        node2._prev_node = node3
        self.assertEqual(node3, node.prev_sibling(lambda x: x.tag == 'h2'))

    def test_node_has_attr(self):
        node = Node('div', attrs=[('class', 'some')])
        self.assertTrue('class_' in node)
        self.assertFalse('src' in node)

    def test_node_inner_text(self):
        node = Node('h1', text='test')
        node2 = Node('h2', text='text')
        node3 = Node('h2', text='txt')
        node4 = Node('h3', text='tort')
        node5 = Node('h3', text='a')
        node._children = Nodes(node2, node3)
        node3._children = Nodes(node4)
        node2._children = Nodes(node5)
        self.assertEqual(['test', 'text', 'a', 'txt', 'tort'], node.inner_text())

    def test_node_inner_text_no_children(self):
        node = Node('h1', text='test')
        self.assertEqual(['test'], node.inner_text())

    def test_node_inner_text_no_texts(self):
        node = Node('h1')
        self.assertEqual([], node.inner_text())

    def test_attr_works_if_from_flatten(self):
        self.assertEqual(Nodes(), Node('div')._flatten(lambda e: e.wrong == 'test'))

    def test_attr_fails_if_not_from_flatten(self):
        with self.assertRaises(AttributeError):
            Node('div').wrong

    def test_children_empty_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node._parent = node2
        node2._children.append(node)
        self.assertEqual(Nodes(node), node2.children())

    def test_children_empty(self):
        node = Node('div')
        self.assertTrue(node.children().is_empty())

    def test_children_empty_filter_no_descendants(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('h2')
        node._parent = node2
        node3._parent = node
        node2._children.append(node)
        node._children.append(node3)
        self.assertEqual(Nodes(node), node2.children())

    def test_children_with_filter(self):
        node = Node('div')
        node2 = Node('h1')
        node3 = Node('h2')
        node._parent = node2
        node3._parent = node2
        node2._children.append(node)
        node2._children.append(node3)
        self.assertEqual(Nodes(node3), node2.children(lambda e: e.tag == 'h2'))


if __name__ == '__main__':
    main()
