from unittest import TestCase, main
from py_parse.helpers import get_filter_body


class HelperTest(TestCase):
    def test_get_filter_body_simple(self):
        lam = lambda x: x + 1
        self.assertEqual('x + 1', get_filter_body(lam))

    def test_get_filter_body_compl(self):
        lam = lambda x: x + 1 and 'text' in x or str(1) == x
        self.assertEqual("x + 1 and 'text' in x or str(1) == x", get_filter_body(lam))

    def test_get_filter_body_compl2(self):
        lam = lambda x: x + 1 and 'text' in x or x == str(1)
        self.assertEqual("x + 1 and 'text' in x or x == str(1)", get_filter_body(lam))

    def test_get_filter_body_func(self):
        def _(x):
            return x + 1

        self.assertEqual("", get_filter_body(_))


if __name__ == '__main__':
    main()
